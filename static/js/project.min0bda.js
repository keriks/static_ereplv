window.COUNTRY_NAMES = {
  "1": "Romania",
  "9": "Brazil",
  "10": "Italy",
  "11": "France",
  "12": "Germany",
  "13": "Hungary",
  "14": "China",
  "15": "Spain",
  "23": "Canada",
  "24": "USA",
  "26": "Mexico",
  "27": "Argentina",
  "28": "Venezuela",
  "29": "United Kingdom",
  "30": "Switzerland",
  "31": "Netherlands",
  "32": "Belgium",
  "33": "Austria",
  "34": "Czech Republic",
  "35": "Poland",
  "36": "Slovakia",
  "37": "Norway",
  "38": "Sweden",
  "39": "Finland",
  "40": "Ukraine",
  "41": "Russia",
  "42": "Bulgaria",
  "43": "Turkey",
  "44": "Greece",
  "45": "Japan",
  "47": "South Korea",
  "48": "India",
  "49": "Indonesia",
  "50": "Australia",
  "51": "South Africa",
  "52": "Republic of Moldova",
  "53": "Portugal",
  "54": "Ireland",
  "55": "Denmark",
  "56": "Iran",
  "57": "Pakistan",
  "58": "Israel",
  "59": "Thailand",
  "61": "Slovenia",
  "63": "Croatia",
  "64": "Chile",
  "65": "Serbia",
  "66": "Malaysia",
  "67": "Philippines",
  "68": "Singapore",
  "69": "Bosnia and Herzegovina",
  "70": "Estonia",
  "71": "Latvia",
  "72": "Lithuania",
  "73": "North Korea",
  "74": "Uruguay",
  "75": "Paraguay",
  "76": "Bolivia",
  "77": "Peru",
  "78": "Colombia",
  "79": "Republic of Macedonia (FYROM)",
  "80": "Montenegro",
  "81": "Republic of China (Taiwan)",
  "82": "Cyprus",
  "83": "Belarus",
  "84": "New Zealand",
  "164": "Saudi Arabia",
  "165": "Egypt",
  "166": "United Arab Emirates",
  "167": "Albania",
  "168": "Georgia",
  "169": "Armenia",
  "170": "Nigeria",
  "171": "Cuba"
};
window.chartColors = {
  red: 'rgb(255, 99, 132)',
  orange: 'rgb(255, 159, 64)',
  yellow: 'rgb(255, 205, 86)',
  green: 'rgb(87, 217, 108)',
  cyan: 'rgb(75, 192, 192)',
  blue: 'rgb(54, 162, 235)',
  purple: 'rgb(153, 102, 255)',
  grey: 'rgb(201, 203, 207)'
};

function intToRGB(str) {
  let hash = 0;
  for (let i = 0; i < str.length; i++) {
    hash = str.charCodeAt(i) + ((hash << 5) - hash);
  }
  let c = (hash & 0x00FFFFFF)
    .toString(16)
    .toUpperCase();

  return "#" + "00000".substring(0, 6 - c.length) + c;
}

function getColor(sString = "") {
  if (sString)
    return intToRGB(sString);
  else {

    let r, g, b, eq = Math.floor(Math.random() * 7 + 1), coof = Math.floor(Math.random() * 16);
    r = (eq & 4 ? coof : Math.floor(Math.random() * 16)) * 16;
    g = (eq & 2 ? coof : Math.floor(Math.random() * 16)) * 16;
    b = (eq & 1 ? coof : Math.floor(Math.random() * 16)) * 16;
    console.log(`eq: ${eq} = rgb(${r}, ${g}, ${b})`);
    return `rgb(${r}, ${g}, ${b})`;
  }
}

function init() {
  $('[data-toggle="tooltip"]').tooltip();
}

window.addEventListener('load', init);

function thousandSeparateNumber(value, digits = 2) {
  let x = Number(value).toFixed(digits).toString();
  let rgx = /(\d+)(\d{3})/;
  while (rgx.test(x))
    x = x.replace(rgx, '$1' + ' ' + '$2');
  return x;
}

function getDateFromEday(eday) {
  let date = new Date(1195509600000 + eday * 86400000);
  let year = date.getFullYear();
  let month = ("0" + date.getMonth()).slice(-2)
  let day = ("0" + date.getDate()).slice(-2)
  return `${year}-${month}-${day}`;
  // return date.toLocaleDateString();
}

const sortObjByKey = fieldName => {
  const sortFn = objToSort => objToSort[fieldName];
  return (curVal, nextVal) => {
    return -1 * (((curVal = sortFn(curVal)) > (nextVal = sortFn(nextVal))) - (nextVal > curVal))
  }
};
const toTitleCase = (str) => str.toLowerCase().split(' ').map(function (word) {
  return (word.charAt(0).toUpperCase() + word.slice(1));
}).join(' ');


if (document.getElementById("id_formWrapper")) {
  window.production = {
    industryData: {
      foodraw: [{base: 0.35, rawUsage: 1, employees: 0}, {base: 0.7, rawUsage: 1, employees: 0}, {base: 1.25, rawUsage: 1, employees: 1}, {base: 1, rawUsage: 1.75, employees: 1}, {base: 2.5, rawUsage: 1, employees: 4}],
      weaponsraw: [{base: 0.35, rawUsage: 1, employees: 0}, {base: 0.7, rawUsage: 1, employees: 0}, {base: 1.25, rawUsage: 1, employees: 1}, {base: 1, rawUsage: 1.75, employees: 1}, {base: 2.5, rawUsage: 1, employees: 4}],
      houseraw: [{base: 0.35, rawUsage: 1, employees: 1}, {base: 0.7, rawUsage: 1, employees: 2}, {base: 1.25, rawUsage: 1, employees: 3}, {base: 1, rawUsage: 1.75, employees: 4}, {base: 2.5, rawUsage: 1, employees: 5}],
      aircraftraw: [{base: 0.35, rawUsage: 1, employees: 1}, {base: 0.7, rawUsage: 1, employees: 2}, {base: 1.25, rawUsage: 1, employees: 3}, {base: 1, rawUsage: 1.75, employees: 4}, {base: 2.5, rawUsage: 1, employees: 5}],
      food: [{base: 100, rawUsage: -0.01, employees: 1}, {base: 100, rawUsage: -0.02, employees: 2}, {base: 100, rawUsage: -0.03, employees: 3}, {base: 100, rawUsage: -0.04, employees: 5}, {base: 100, rawUsage: -0.05, employees: 10}, {base: 100, rawUsage: -0.06, employees: 10}, {base: 100, rawUsage: -0.2, employees: 10}],
      weapons: [{base: 10, rawUsage: -0.1, employees: 1}, {base: 10, rawUsage: -0.2, employees: 2}, {base: 10, rawUsage: -0.3, employees: 3}, {base: 10, rawUsage: -0.4, employees: 5}, {base: 10, rawUsage: -0.5, employees: 10}, {base: 10, rawUsage: - 0.6, employees: 10}, {base: 10, rawUsage: -2, employees: 10}],
      house: [{base: 0.2, rawUsage: -10, employees: 1}, {base: 0.1, rawUsage: -20, employees: 2}, {base: 0.05, rawUsage: -40, employees: 3}, {base: 0.025, rawUsage: -80, employees: 5}, {base: 1 / 60, rawUsage: -120, employees: 10}],
      aircraft: [{base: 5, rawUsage: -0.2, employees: 1}, {base: 5, rawUsage: -0.4, employees: 2}, {base: 5, rawUsage: -0.6, employees: 3}, {base: 5, rawUsage: -0.8, employees: 5}, {base: 5, rawUsage: -1, employees: 10}]
    },
    noWamIndustries: ["houseraw", "aircraftraw", "house", "aircraft"],
    countrySalaries: {}
  }
  fetch(`//${window.location.host}/production.json`)
    .then((response) => response.json())
    .then((response) => {
      window.countryData = [];
      for (let row of response.data) {
        let countryRow = {id: row.id, name: row.name, averageSalary: row.avg_salary, workTax: row.work_tax};
        window.production.countrySalaries[countryRow.id] = countryRow;
        window.countryData.push(countryRow);
      }
      window.countryData = response.data;
    }).catch((ex) => {
    alert("Error occurred. Try again later.");
    if (window.console) console.log('failed submitting', ex)
  });
}

function addHolding() {
  let target = document.getElementById("id_holdingWrapper");
  let holdingNr = (document.getElementById("id_holdingWrapper").children.length + 1).toString();
  let holdingId = `id_holding_${holdingNr}`;
  let holdingWrapper = document.createElement("div");
  holdingWrapper.className = "card my-2";
  holdingWrapper.id = holdingId;
  holdingWrapper.appendChild(createCardHeader(`Holding ${holdingNr}`, 4));
  let cardBody = document.createElement("div");
  cardBody.className = "card-body p-2";
  cardBody.appendChild(createHoldingCountryForm(holdingId));
  cardBody.appendChild(createHoldingBonusAccordion(holdingId));
  cardBody.appendChild(createHoldingCompanyWrapper(holdingId));
  cardBody.appendChild(createHoldingTableHeader());
  cardBody.appendChild(createHoldingTable(holdingId));
  holdingWrapper.appendChild(cardBody);
  target.appendChild(holdingWrapper)
}

function addCompany(event) {
  let button = event.target;
  let companyWrapper = button.parentElement;
  let companyId = `${companyWrapper.id}_${companyWrapper.children.length - 1}`;

  let holdingTable = document.getElementById(`${button.dataset.holdingId}_table`);
  holdingTable.appendChild(createCompanyTableRow(companyId));

  let formRow = document.createElement("div");
  formRow.className = "form-row my-2";
  formRow.id = companyId;

  formRow.appendChild(createCompanyTypeSelectRow(companyId));
  formRow.appendChild(createCompanyQualitySelectRow(companyId));
  formRow.appendChild(createCompanyNumberInputRow(companyId, "Count"));
  formRow.appendChild(createCompanyNumberInputRow(companyId, "Employees"));
  formRow.appendChild(createCompanyWamInputRow(companyId));

  companyWrapper.insertBefore(formRow, button);
}

function createCardHeader(title, level) {
  let cardHeader = document.createElement("div");
  cardHeader.className = "card-header py-1 px-1";
  let cardTitle = document.createElement(`h${level}`);
  cardTitle.textContent = title;
  cardHeader.appendChild(cardTitle);
  return cardHeader
}

function createHoldingCountryForm(holdingId) {
  let holdingCountryForm = document.createElement("div");
  holdingCountryForm.className = "form-group row";
  let holdingCountryId = `${holdingId}_country`;
  let holdingCountryLabel = document.createElement("label");
  holdingCountryLabel.for = holdingCountryId;
  holdingCountryLabel.className = "col-sm-3 col-form-label text-right";
  holdingCountryLabel.textContent = "Country";
  holdingCountryForm.appendChild(holdingCountryLabel);

  let holdingCountrySelectDiv = document.createElement("div");
  holdingCountrySelectDiv.className = "col-lg-4 col-sm-6";
  let holdingCountrySelect = document.createElement("select");
  holdingCountrySelect.className = "form-control";
  holdingCountrySelect.id = holdingCountryId;
  let countries = window.countryData.sort(sortObjByKey("name")).reverse();
  for (let country of countries) {
    let option = document.createElement("option");
    option.value = country.id;
    option.textContent = country.name;
    holdingCountrySelect.appendChild(option);
  }
  holdingCountrySelectDiv.appendChild(holdingCountrySelect);
  holdingCountryForm.appendChild(holdingCountrySelectDiv);
  return holdingCountryForm
}

function createHoldingBonusAccordion(holdingId) {
  let bonusId = `${holdingId}_bonuses`;
  let accordionCard = document.createElement("div");
  accordionCard.className = "accordion card mb-2";
  accordionCard.id = `${bonusId}_card`;
  let showHideH5 = document.createElement("h5");
  showHideH5.className = "mb-0"
  let toggleButton = document.createElement("button");
  toggleButton.className = "btn btn-link text-left btn-sm";
  toggleButton.type = "button";
  toggleButton.dataset.toggle = "collapse";
  toggleButton.dataset.target = `#${bonusId}`;
  toggleButton.innerHTML = 'Bonuses <i class="fas fa-eye"></i>';
  showHideH5.appendChild(toggleButton);
  accordionCard.appendChild(showHideH5);

  let wrapper = document.createElement("div");
  wrapper.id = bonusId;
  wrapper.className = "collapse";
  wrapper.dataset.parent = `#${accordionCard.id}`

  let formRow = document.createElement("div");
  formRow.className = "form-group form-row";

  for (let industry of ["FoodRaw", "WeaponsRaw", "HouseRaw", "AircraftRaw", "Food", "Weapons", "House", "Aircraft"]) {
    let industryId = `${bonusId}_input_${industry.toLowerCase()}`;

    let col = document.createElement("div");
    col.className = "col-6 col-sm-4 col-md-3 mb-3";

    let label = document.createElement("label");
    label.for = industryId;
    label.textContent = industry;
    col.appendChild(label);

    let group = document.createElement("div");
    group.className = "input-group";

    let input = document.createElement("input");
    input.type = "number";
    input.min = "75";
    input.max = "300";
    input.step = "0.5";
    input.id = industryId;
    input.className = "form-control";
    input.value = "193";
    input.oninput = updateTableOnBonusChangeEvent;
    group.appendChild(input);

    let groupAppend = document.createElement("div");
    groupAppend.className = "input-group-append";

    let span = document.createElement("span");
    span.className = "input-group-text";
    span.textContent = "%";
    groupAppend.appendChild(span);
    group.appendChild(groupAppend);
    col.appendChild(group);
    formRow.appendChild(col);
  }
  wrapper.appendChild(formRow);
  accordionCard.appendChild(wrapper);
  return accordionCard;
}

function createHoldingCompanyWrapper(holdingId) {
  let wrapperId = `${holdingId}_company`;
  let wrapper = document.createElement("div");
  wrapper.className = "card my-2";
  wrapper.appendChild(createCardHeader("Companies", 5))

  let companyWrapper = document.createElement("div");
  companyWrapper.id = wrapperId;
  companyWrapper.className = "card-body p-2";

  let addCompanyButton = document.createElement("button");
  addCompanyButton.type = "button";
  addCompanyButton.id = `${wrapperId}_addCompany`;
  addCompanyButton.className = "btn btn-sm btn-outline-success mt-2";
  addCompanyButton.onclick = addCompany;
  addCompanyButton.textContent = "+ Add Company";
  addCompanyButton.dataset.holdingId = holdingId;
  addCompanyButton.dataset.wrapperId = wrapperId;
  companyWrapper.appendChild(addCompanyButton);
  wrapper.appendChild(companyWrapper);
  return wrapper;
}

function createHoldingTableHeader() {
  let tableHeaderDiv = document.createElement("div");
  tableHeaderDiv.className = "card-body pb-2";
  let tableHeader = document.createElement("h5");
  tableHeader.textContent = "Holding production";
  tableHeaderDiv.appendChild(tableHeader);
  return tableHeaderDiv;
}

function createHoldingTable(holdingId) {
  let tableDiv = document.createElement("div");
  tableDiv.className = "table-responsive";

  let table = document.createElement("table");
  table.className = "table table-sm table-striped table-hover";

  let tableHead = document.createElement("thead");
  let tableHeadRow = document.createElement("tr");
  for (let title of ["Company", "Products", "Raw usage", "Tax", "Salaries", "Price per item"]) {
    let th = document.createElement("th");
    th.textContent = title;
    tableHeadRow.appendChild(th);
  }
  tableHead.appendChild(tableHeadRow);
  table.appendChild(tableHead)
  let tableBody = document.createElement("tbody");
  tableBody.id = `${holdingId}_table`;
  table.appendChild(tableBody);
  tableDiv.appendChild(table);
  return tableDiv;
}

function createCompanyTypeSelectRow(companyId) {
  let inputId = `${companyId}_type`
  let inputCol = document.createElement("div");
  inputCol.className = "col-6 col-sm-3 col-md-3 col-lg-2";

  let inputLabel = document.createElement("label");
  inputLabel.for = inputId;
  inputLabel.textContent = "Company type:";
  inputCol.appendChild(inputLabel);

  let inputSelect = document.createElement("select");
  inputSelect.id = inputId;
  inputSelect.className = "form-control";
  inputSelect.onchange = updateTableOnCompanyEvent;

  let rawOptGroup = document.createElement("optgroup");
  rawOptGroup.label = "RAW";
  for (let kind of ["FoodRaw", "WeaponsRaw", "HouseRaw", "AircraftRaw"]) {
    let opt = document.createElement("option");
    opt.value = kind.toLowerCase();
    opt.textContent = kind;
    rawOptGroup.appendChild(opt);
  }
  inputSelect.appendChild(rawOptGroup);

  let finalOptGroup = document.createElement("optgroup");
  finalOptGroup.label = "Factory";
  for (let kind of ["Food", "Weapons", "House", "Aircraft"]) {
    let opt = document.createElement("option");
    opt.value = kind.toLowerCase();
    opt.textContent = kind;
    finalOptGroup.appendChild(opt);
  }
  inputSelect.appendChild(finalOptGroup);
  inputCol.appendChild(inputSelect);
  return inputCol;
}

function createCompanyQualitySelectRow(companyId) {
  let inputId = `${companyId}_quality`
  let inputCol = document.createElement("div");
  inputCol.className = "col-6 col-sm-3 col-md-3 col-lg-2 col-xl-1";

  let inputLabel = document.createElement("label");
  inputLabel.for = inputId;
  inputLabel.textContent = "Quality:";
  inputCol.appendChild(inputLabel);

  let inputSelect = document.createElement("select");
  inputSelect.id = inputId;
  inputSelect.className = "form-control";
  inputSelect.onchange = updateTableOnCompanyEvent;

  for (let q = 1; q < 8; q++) {
    let opt = document.createElement("option");
    opt.value = q;
    opt.textContent = `Q${q}`;
    inputSelect.appendChild(opt);
  }
  inputCol.appendChild(inputSelect);
  return inputCol;
}

function createCompanyNumberInputRow(companyId, label) {
  let inputId = `${companyId}_${label.toLowerCase()}`
  let inputCol = document.createElement("div");
  inputCol.className = "col-6 col-sm-3 col-md-3 col-lg-2";

  let inputLabel = document.createElement("label");
  inputLabel.for = inputId;
  inputLabel.textContent = `${label}:`;
  inputCol.appendChild(inputLabel);

  let inputInput = document.createElement("input");
  inputInput.id = inputId;
  inputInput.className = "form-control";
  inputInput.type = "number";
  inputInput.min = "0";
  inputInput.step = "1";
  inputInput.value = "0";
  inputInput.onchange = updateTableOnCompanyEvent;
  inputCol.appendChild(inputInput);
  return inputCol;
}

function createCompanyWamInputRow(companyId) {
  let inputId = `${companyId}_wam`
  let inputCol = document.createElement("div");
  inputCol.className = "col-6 col-sm-3 col-md-3 col-lg-2";

  let inputLabel = document.createElement("label");

  let labelText = document.createTextNode("Work as Manager");
  let inputInput = document.createElement("input");
  inputInput.id = inputId;
  inputInput.className = "form-check-inline";
  inputInput.type = "checkbox";
  inputInput.onchange = updateTableOnCompanyEvent;
  inputLabel.appendChild(inputInput);
  inputLabel.appendChild(labelText);
  inputCol.appendChild(inputLabel);

  return inputCol;
}

function createCompanyTableRow(companyId) {
  let rowId = `${companyId}_table_row`
  let row = document.createElement("tr");
  row.id = rowId;

  for (let title of ["company", "products", "rawusage", "taxes", "salaries", "ppi"]) {
    let td = document.createElement("td");
    td.id = `${rowId}_${title}`;
    row.appendChild(td);
  }

  return row;
}

function updateCompanyTableData(companyId) {
  let holdingId = companyId.split("_", 3).join("_");
  let companyTableRowId = `${companyId}_table_row`;
  let industry = document.getElementById(`${companyId}_type`).value;
  let quality = document.getElementById(`${companyId}_quality`).value;
  let count = parseInt(document.getElementById(`${companyId}_count`).value);
  let employees = parseInt(document.getElementById(`${companyId}_employees`).value);
  let wam = document.getElementById(`${companyId}_wam`).checked;
  wam = window.production.noWamIndustries.indexOf(industry) === -1 ? wam : false;

  let trName = document.getElementById(`${companyTableRowId}_company`);
  let trProducts = document.getElementById(`${companyTableRowId}_products`);
  let trRawUsage = document.getElementById(`${companyTableRowId}_rawusage`);
  let trTaxes = document.getElementById(`${companyTableRowId}_taxes`);
  let trSalaries = document.getElementById(`${companyTableRowId}_salaries`);
  let trPPI = document.getElementById(`${companyTableRowId}_ppi`);
  trName.textContent = `${toTitleCase(industry)} q${quality}`;

  let citizenship = window.production.countrySalaries[document.getElementById("id_formWrapper_select_citizenship").value];
  let country = window.production.countrySalaries[document.getElementById(`${holdingId}_country`).value];
  let bonus = parseInt(document.getElementById(`${holdingId}_bonuses_input_${industry}`).value) / 100;
  let productionData = window.production.industryData[industry][parseInt(quality)-1];
  if (productionData === undefined) productionData = {base: 0, rawUsage: 0, employees: 0};
  let products = productionData.base * bonus;
  let maxEmployees = productionData.employees * count;
  if (employees > maxEmployees) employees = maxEmployees;
  let totalProducts = (products * (wam ? count : 0) + products * employees).toFixed(2)
  trProducts.textContent = totalProducts.toString();
  trRawUsage.textContent = (totalProducts * productionData.rawUsage).toFixed(2).toString();
  let taxPerWam = country.averageSalary * 0.8 * (country.workTax / 100) + citizenship.averageSalary * 0.2 * (citizenship.workTax / 100);
  let taxes = wam ? count * taxPerWam : 0;
  trTaxes.textContent = taxes.toFixed(3).toString();
  let salaries = employees * parseFloat(document.getElementById("id_formWrapper_average_salary").value).toFixed(2)
  trSalaries.textContent = salaries.toString();
  let ppi = ((taxes + salaries) / totalProducts).toFixed(3);
  trPPI.textContent = ppi.toString();
}

function updateTableOnCompanyEvent(event) {
  let companyId = event.target.id.split("_", 5).join("_");
  updateCompanyTableData(companyId);
}

function updateTableOnBonusChangeEvent(event) {
  let holdingId = event.target.id.split("_", 3).join("_");
  for (let companyNr = 0; companyNr < document.getElementById(`${holdingId}_company`).children.length - 1; companyNr++)
    updateCompanyTableData(`${holdingId}_company_${companyNr}`);
}

function updateAllTablesEvent(event) {
  for (let holdingNr = 0; holdingNr < document.getElementById("id_holdingWrapper").children.length; holdingNr++) {
    let holdingId = `id_holding_${holdingNr + 1}`;
    for (let companyNr = 0; companyNr < document.getElementById(`${holdingId}_company`).children.length - 1; companyNr++)
      updateCompanyTableData(`${holdingId}_company_${companyNr}`);
  }
}

const countrySelect = (ev) => {
  window.location.href = `/country/account/${document.getElementById("id_selectCountry").value}`
}

function onClickChangeChartType(ev) {
  let btn = ev.target;
  let chartWrapper = document.getElementById('id_chartButtons');
  chartWrapper.dataset.field = btn.dataset.value;
  let prevBtn = document.querySelector('#id_chartButtons > button.active');
  if (prevBtn) prevBtn.classList.remove("active");
  btn.classList.add("active");
  updateCountryAccountChart();
}

function updateCountryAccountChart() {
  let wrapper = document.getElementById('id_chartButtons');
  let field = wrapper.dataset.field;
  let fieldTitleMap = {cc: "Currency", gold: "Gold", salary: "Average salary", food: "Energy"};

  fetch(document.location.href + `${field}.json`
  ).then(function (response) {
    return response.json()
  }).then(function (response) {
    if (response.status) {
      if (response.hasOwnProperty("data")) {
        let dataset_base = Object.assign({}, window.dataset_template);
        dataset_base.data = response.data[field];
        dataset_base.label = fieldTitleMap[field];
        window.config.data.datasets = [];
        window.config.data.labels = [];
        for (let i = 0; i < response.data.timestamp.length; i++)
          window.config.data.labels.push(new Date(response.data.timestamp[i]))
        window.config.data.datasets.push(dataset_base);
        window.config.options.scales.yAxes[0] = {beginAtZero: false, ticks: {}};
        window.chart.update();
      }
    }
  }).catch(function (ex) {
    alert("Error occurred. Try again later.");
    if (window.console)
      console.log('failed submitting', ex)
  });
}

if (document.getElementById("id_view_country-account")) {

  let ctx = document.getElementById("myChart");
  window.dataset_template = {
    backgroundColor: [window.chartColors.green],
    borderColor: [window.chartColors.green],
    label: "",
    data: [],
    fill: false,
    lineTension: 0,
  };
  window.config = {
    type: 'line',
    data: {
      labels: [],
      lineTension: 1,
      datasets: [Object.assign({}, window.dataset_template)]
    },
    options: {
      responsive: true,
      tooltips: {mode: 'x', intersect: false,},
      scales: {
        xAxes: [{type: 'time', time: {displayFormats: {'hour': 'DD.MM HH:mm',}, tooltipFormat: 'YYYY.MM.DD HH:mm'}, scaleLabel: {display: true,}}],
        yAxes: [{scaleLabel: {display: true}, ticks: {beginAtZero: false}}]
      }
    }
  };
  window.chart = new Chart(ctx, window.config);
  for (let btn of document.querySelectorAll("#id_chartButtons > button"))
    btn.onclick = onClickChangeChartType;
  updateCountryAccountChart();
}

const calculateLevel = e => e >= 1e4 ? 25 + e / 5e3 : e >= 7e3 ? 26 : e >= 3e3 ? 24 + (e - 3e3) / 2e3 : e >= 2e3 ? 23 : e >= 500 ? 19 + e / 500 : e >= 450 ? 19 : e >= 370 ? 17 + (e - 370) / 40 : e >= 300 ? 15 + (e - 300) / 35 : e >= 150 ? 10 + (e - 150) / 30 : e >= 50 ? 5 + (e - 50) / 20 : e >= 20 ? 3 + (e - 20) / 15 : 1 + e / 10;
const getLevel = e => parseInt(calculateLevel(e));
const getGroundRankId = e => {
    let rankId = 1;
    for (let a of GroundRanks) e >= a.value && rankId < a.id && (rankId = a.id);
    return rankId
};
const getAirRankId = e => {
    let rankId = 1;
    for (let a of AirRanks) e >= a.value && rankId < a.id && (rankId = a.id);
    return rankId
};
const getHitAmount = (r, isAir, isMax) => {
    let rankId = isAir ? r.air_rank_id : r.ground_rank_id,
        i = 10 * (1 + (isAir ? 0 : r.str) / 400) * (1 + rankId / 5) * (1 + (isAir ? isMax ? 100 : 0 : 200) / 100),
		elite = r.xp > 380000;
	if (elite) i*=1.1;
	if (isMax) return i *= 1.1, isAir ? i *= 1.5 : (rankId > 69 && (i = i * (100 + 10 * (rankId - 69)) / 100), i *= 2);
	return i;
};
const fieldMapping = {
    xp: "Experience",
    rp: "Ground Rank",
    arp: "Aircraft Rank",
    str: "Ground Strength",
	hit: "Ground Hit",
    bh: "Battle Hero",
    sh: "Sky Hero",
    tp: "True Patriot",
    ch: "Campaign Hero",
    rh: "Resistance Hero",
    mm: "Mercenary Medal",
    ff: "Freedom Fighter"
};
const GroundRanks = [
    {id: 1, name: "Recruit", value: 0},
    {id: 2, name: "Private", value: 15},
    {id: 3, name: "Private*", value: 45},
    {id: 4, name: "Private**", value: 80},
    {id: 5, name: "Private***", value: 120},
    {id: 6, name: "Corporal", value: 170},
    {id: 7, name: "Corporal*", value: 250},
    {id: 8, name: "Corporal**", value: 350},
    {id: 9, name: "Corporal***", value: 450},
    {id: 10, name: "Sergeant", value: 600},
    {id: 11, name: "Sergeant*", value: 800},
    {id: 12, name: "Sergeant**", value: 1e3},
    {id: 13, name: "Sergeant***", value: 1400},
    {id: 14, name: "Lieutenant", value: 1850},
    {id: 15, name: "Lieutenant*", value: 2350},
    {id: 16, name: "Lieutenant**", value: 3e3},
    {id: 17, name: "Lieutenant***", value: 3750},
    {id: 18, name: "Captain", value: 5e3},
    {id: 19, name: "Captain*", value: 6500},
    {id: 20, name: "Captain**", value: 9e3},
    {id: 21, name: "Captain***", value: 12e3},
    {id: 22, name: "Major", value: 15500},
    {id: 23, name: "Major*", value: 2e4},
    {id: 24, name: "Major**", value: 25e3},
    {id: 25, name: "Major***", value: 31e3},
    {id: 26, name: "Commander", value: 4e4},
    {id: 27, name: "Commander*", value: 52e3},
    {id: 28, name: "Commander**", value: 67e3},
    {id: 29, name: "Commander***", value: 85e3},
    {id: 30, name: "Lt Colonel", value: 11e4},
    {id: 31, name: "Lt Colonel*", value: 14e4},
    {id: 32, name: "Lt Colonel**", value: 18e4},
    {id: 33, name: "Lt Colonel***", value: 225e3},
    {id: 34, name: "Colonel", value: 285e3},
    {id: 35, name: "Colonel*", value: 355e3},
    {id: 36, name: "Colonel**", value: 435e3},
    {id: 37, name: "Colonel***", value: 54e4},
    {id: 38, name: "General", value: 66e4},
    {id: 39, name: "General*", value: 8e5},
    {id: 40, name: "General**", value: 95e4},
    {id: 41, name: "General***", value: 114e4},
    {id: 42, name: "Field Marshal", value: 135e4},
    {id: 43, name: "Field Marshal*", value: 16e5},
    {id: 44, name: "Field Marshal**", value: 1875e3},
    {id: 45, name: "Field Marshal***", value: 2185e3},
    {id: 46, name: "Supreme Marshal", value: 255e4},
    {id: 47, name: "Supreme Marshal*", value: 3e6},
    {id: 48, name: "Supreme Marshal**", value: 35e5},
    {id: 49, name: "Supreme Marshal***", value: 415e4},
    {id: 50, name: "National Force", value: 49e5},
    {id: 51, name: "National Force*", value: 58e5},
    {id: 52, name: "National Force**", value: 7e6},
    {id: 53, name: "National Force***", value: 9e6},
    {id: 54, name: "World Class Force", value: 115e5},
    {id: 55, name: "World Class Force*", value: 145e5},
    {id: 56, name: "World Class Force**", value: 18e6},
    {id: 57, name: "World Class Force***", value: 22e6},
    {id: 58, name: "Legendary Force", value: 265e5},
    {id: 59, name: "Legendary Force*", value: 315e5},
    {id: 60, name: "Legendary Force**", value: 37e6},
    {id: 61, name: "Legendary Force***", value: 43e6},
    {id: 62, name: "God of War", value: 5e7},
    {id: 63, name: "God of War*", value: 1e8},
    {id: 64, name: "God of War**", value: 2e8},
    {id: 65, name: "God of War***", value: 5e8},
    {id: 66, name: "Titan", value: 1e9},
    {id: 67, name: "Titan*", value: 2e9},
    {id: 68, name: "Titan**", value: 4e9},
    {id: 69, name: "Titan***", value: 1e10},
    {id: 70, name: "Legends I", value: 2e10},
    {id: 71, name: "Legends II", value: 3e10},
    {id: 72, name: "Legends III", value: 4e10},
    {id: 73, name: "Legends IV", value: 5e10},
    {id: 74, name: "Legends V", value: 6e10},
    {id: 75, name: "Legends VI", value: 7e10},
    {id: 76, name: "Legends VII", value: 8e10},
    {id: 77, name: "Legends VIII", value: 9e10},
    {id: 78, name: "Legends IX", value: 1e11},
    {id: 79, name: "Legends X", value: 11e10},
    {id: 80, name: "Legends XI", value: 12e10},
    {id: 81, name: "Legends XII", value: 13e10},
    {id: 82, name: "Legends XIII", value: 14e10},
    {id: 83, name: "Legends XIV", value: 15e10},
    {id: 84, name: "Legends XV", value: 16e10},
    {id: 85, name: "Legends XVI", value: 17e10},
    {id: 86, name: "Legends XVII", value: 18e10},
    {id: 87, name: "Legends XVIII", value: 19e10},
    {id: 88, name: "Legends XIX", value: 2e11},
    {id: 89, name: "Legends XX", value: 21e10}];
const AirRanks = [
    {id: 1, name: "Airman", value: 0},
    {id: 2, name: "Airman 1st Class", value: 10},
    {id: 3, name: "Airman 1st Class*", value: 25},
    {id: 4, name: "Airman 1st Class**", value: 45},
    {id: 5, name: "Airman 1st Class***", value: 70},
    {id: 6, name: "Airman 1st Class****", value: 100},
    {id: 7, name: "Airman 1st Class*****", value: 140},
    {id: 8, name: "Senior Airman", value: 190},
    {id: 9, name: "Senior Airman*", value: 270},
    {id: 10, name: "Senior Airman**", value: 380},
    {id: 11, name: "Senior Airman***", value: 530},
    {id: 12, name: "Senior Airman****", value: 850},
    {id: 13, name: "Senior Airman*****", value: 1300},
    {id: 14, name: "Staff Sergeant", value: 2340},
    {id: 15, name: "Staff Sergeant*", value: 3300},
    {id: 16, name: "Staff Sergeant**", value: 4200},
    {id: 17, name: "Staff Sergeant***", value: 5150},
    {id: 18, name: "Staff Sergeant****", value: 6100},
    {id: 19, name: "Staff Sergeant*****", value: 7020},
    {id: 20, name: "Aviator", value: 9100},
    {id: 21, name: "Aviator*", value: 12750},
    {id: 22, name: "Aviator**", value: 16400},
    {id: 23, name: "Aviator***", value: 2e4},
    {id: 24, name: "Aviator****", value: 23650},
    {id: 25, name: "Aviator*****", value: 27300},
    {id: 26, name: "Flight Lieutenant", value: 35500},
    {id: 27, name: "Flight Lieutenant*", value: 48e3},
    {id: 28, name: "Flight Lieutenant**", value: 6e4},
    {id: 29, name: "Flight Lieutenant***", value: 72400},
    {id: 30, name: "Flight Lieutenant****", value: 84500},
    {id: 31, name: "Flight Lieutenant*****", value: 97e3},
    {id: 32, name: "Squadron Leader", value: 11e4},
    {id: 33, name: "Squadron Leader*", value: 14e4},
    {id: 34, name: "Squadron Leader**", value: 17e4},
    {id: 35, name: "Squadron Leader***", value: 21e4},
    {id: 36, name: "Squadron Leader****", value: 29e4},
    {id: 37, name: "Squadron Leader*****", value: 35e4},
    {id: 38, name: "Chief Master Sergeant", value: 429e3},
    {id: 39, name: "Chief Master Sergeant*", value: 601e3},
    {id: 40, name: "Chief Master Sergeant**", value: 772e3},
    {id: 41, name: "Chief Master Sergeant***", value: 944e3},
    {id: 42, name: "Chief Master Sergeant****", value: 1115e3},
    {id: 43, name: "Chief Master Sergeant*****", value: 1287e3},
    {id: 44, name: "Wing Commander", value: 1673e3},
    {id: 45, name: "Wing Commander*", value: 2238e3},
    {id: 46, name: "Wing Commander**", value: 2804e3},
    {id: 47, name: "Wing Commander***", value: 3369e3},
    {id: 48, name: "Wing Commander****", value: 3935e3},
    {id: 49, name: "Wing Commander*****", value: 45e5},
    {id: 50, name: "Group Captain", value: 502e4},
    {id: 51, name: "Group Captain*", value: 7028e3},
    {id: 52, name: "Group Captain**", value: 9036e3},
    {id: 53, name: "Group Captain***", value: 11044e3},
    {id: 54, name: "Group Captain****", value: 13052e3},
    {id: 55, name: "Group Captain*****", value: 1506e4},
    {id: 56, name: "Air Commodore", value: 1958e4},
    {id: 57, name: "Air Commodore*", value: 27412e3},
    {id: 58, name: "Air Commodore**", value: 35244e3},
    {id: 59, name: "Air Commodore***", value: 43076e3},
    {id: 60, name: "Air Commodore****", value: 50908e3},
    {id: 61, name: "Air Commodore*****", value: 5874e4},
    {id: 62, name: "Air Vice Marshal", value: 7636e4},
    {id: 63, name: "Air Vice Marshal*", value: 113166443},
    {id: 64, name: "Air Vice Marshal**", value: 137448e3}
];


if (document.getElementById('id_view_ranking')) {
    window.addEventListener('load', function () {
        fetch(`//${window.location.host}/ranking.json`).then((response) => response.json()).then((response) => {
            for (let row of response.data) {
                let rankId = getGroundRankId(row.rp);
                row.ground_rank_name = GroundRanks[rankId-1].name;
                row.ground_rank_id = rankId;
                rankId = getAirRankId(row.arp);
                row.air_rank_name = AirRanks[rankId-1].name;
                row.air_rank_id = rankId;
                row.groundBaseHit = getHitAmount(row, false, 0);
                row.groundMaxHit = getHitAmount(row, false, 1);
                row.hit = getHitAmount(row, false, 1);
                row.airBaseHit = getHitAmount(row, true, 0);
                row.airMaxHit = getHitAmount(row, true, 1);
            }
            window.player_data = response.data;
        }).catch((ex) => {
            alert("Error occurred. Try again later.");
            if (window.console) console.log('failed submitting', ex)
            document.getElementById('id_table_container').classList.add('d-none');
        });
        let button = document.getElementById('id_refresh_ranking');
        button.onclick = (ev) => {
            if (window.console) console.debug('button onclick Event -> ', ev)
            let field = document.getElementById('id_field').value;
            if (field === 'rp') window.player_data.sort(sortObjByKey('groundMaxHit'));
            let data = window.player_data.sort(sortObjByKey(field));
            let table = document.getElementById('id_detail_table');
            let thead = table.querySelector('thead');
            thead.innerHTML = "";
            let trHead = document.createElement('tr');
            let thNpk = document.createElement('th');
            let thName = document.createElement('th');
            thNpk.textContent = "#";
            thName.textContent = "Spēlētajs";
            trHead.appendChild(thNpk);
            trHead.appendChild(thName);
            if (field === 'rp' || field === 'arp') {
                let thRank = document.createElement('th');
                let thHit = document.createElement('th');
                thRank.textContent = "Rank";
                thHit.textContent = "Max Hit";
                trHead.appendChild(thRank);
                trHead.appendChild(thHit);
            }
            let thField = document.createElement('th');
            thField.textContent = fieldMapping[field] || field;
            trHead.appendChild(thField);
            if (field === 'xp') {
                let thLevel = document.createElement('th');
                thLevel.textContent = 'Level';
                trHead.appendChild(thLevel);
            }
            thead.appendChild(trHead);
            let tbody = table.querySelector('tbody');
            tbody.innerHTML = "";
            let npk = 1;
            for (let row of data) {
                let tr = document.createElement('tr');
                let tdNpk = document.createElement('th');
                tdNpk.textContent = (npk++).toString();
                tr.appendChild(tdNpk);
                let tdName = document.createElement('th');
                tdName.textContent = row.name;
                tr.appendChild(tdName);
                if (field === 'rp' || field === 'arp') {
                    let tdRank = document.createElement('td');
                    let tdHit = document.createElement('td');
                    tdRank.textContent = field === 'rp' ? row.ground_rank_name : row.air_rank_name;
                    tdRank.classList.add('text-nowrap');
                    tdHit.classList.add('text-nowrap');
                    let baseHit = row[field === 'arp' ? 'airBaseHit' : 'groundBaseHit'];
                    let maxHit = row[field === 'arp' ? 'airMaxHit' : 'groundMaxHit'];
                    let strong = document.createElement('strong');
                    let br = document.createElement('br');
                    let small = document.createElement('small');
                    strong.textContent = `Max hit: ${thousandSeparateNumber(maxHit, 0)}`;
                    small.textContent = `Base: ${thousandSeparateNumber(baseHit, 0)}`;
                    tdHit.appendChild(strong);
                    tdHit.appendChild(br);
                    tdHit.appendChild(small);
                    tr.appendChild(tdRank);
                    tr.appendChild(tdHit);
                }
                let td = document.createElement('td');
                td.textContent = thousandSeparateNumber(row[field], 0);
                td.classList.add('text-right');
                td.classList.add('text-monospace');
                td.classList.add('text-nowrap');
                tr.appendChild(td);
                if (field === 'xp') {
                    let tdLevel = document.createElement('th');
                    tdLevel.classList.add('text-right');
                    tdLevel.classList.add('text-monospace');
                    tdLevel.classList.add('text-nowrap');
                    tdLevel.textContent = thousandSeparateNumber(getLevel(row.xp), 0);
                    tr.appendChild(tdLevel);
                }
                tbody.appendChild(tr);
            }
            document.getElementById('id_table_container').classList.remove('d-none');
        }
    });
}

if (document.getElementById('id_view_taxes')) {
    window.addEventListener('load', function () {
        window.baseColours = {
            "Work as Manager": 'rgb(255, 99, 132)',
            "Treasury Donations": 'rgb(255, 159, 64)',
            "Value Added Tax": 'rgb(255, 205, 86)',
            "Work": 'rgb(75, 192, 192)',
            "Medals": 'rgb(54, 162, 235)',
            "Import Tax": 'rgb(153, 102, 255)',
            "Cities Subsidy": 'rgb(201, 203, 207)',
            "Resource Concession Fee": 'rgb(87, 217, 108)',
        };
        window.config = {
            type: 'line',
            data: {
                labels: [],
                lineTension: 1,
                datasets: [Object.assign({}, window.dataset_template)]
            },
            options: {
                responsive: true,
                tooltips: {mode: 'x', intersect: false,},
                scales: {
                    xAxes: [{display: true, stacked: false, scaleLabel: {display: true, labelString: 'eDay'}}],
                    yAxes: [{display: true, stacked: false, scaleLabel: {display: true, labelString: 'Currency'}}]
                    // xAxes: [{display: true, stacked: true, scaleLabel: {display: true, labelString: 'eDay'}}],
                    // yAxes: [{display: true, stacked: true, scaleLabel: {display: true, labelString: 'Currency'}}]
                },
                animation: {
                    onComplete: function (animation) {
                        let a = document.getElementById("id_a_chart_image");
                        a.href = window.chart.toBase64Image();
                    }
                }
            }
        };

        let ctx = document.getElementById("myChart").getContext('2d');
        window.chart = new Chart(ctx, window.config);
        window.chart.update()


        if (document.getElementById('id_get_country_data_button')) {
            let button = document.getElementById('id_get_country_data_button');
            button.onclick = (ev) => {
                let country_id = document.getElementById('id_country_select').value;
                history.pushState(
                    {countryId: country_id, fromDay: 3731, tillDay: 5236},
                    `${window.COUNTRY_NAMES[country_id]} d3731-d5236`,
                    `${ev.target.dataset.baseHref}${country_id}/`
                );
                // Update chart
                fetch(
                    `//${window.location.host}${ev.target.dataset.baseHref}${country_id}/columns.json`
                ).then((response) => response.json()).then(function (response) {
                    let datasets = [];
                    for (let i = 0; i < response.data.dataColumns.length; i++) {
                        let col = response.data.dataColumns[i];
                        datasets.push({
                            backgroundColor: window.baseColours[col] ? window.baseColours[col] : getColor(col),
                            borderColor: window.baseColours[col] ? window.baseColours[col] : getColor(col),
                            label: col,
                            data: response.data[col],
                            fill: false,
                            lineTension: 0
                        })
                    }
                    window.config.data.datasets = datasets;
                    window.config.data.labels = response.data.eDay;
                    window.chart.update();
                }).catch(function (ex) {
                    alert("Error occurred. Try again later.");
                    if (window.console)
                        console.log('failed submitting', ex)
                });

                // Fetch totals
                fetch(
                    `//${window.location.host}${ev.target.dataset.baseHref}${country_id}/total.json`
                ).then((response) => response.json()).then(function (response) {
                    if (response.status) {
                        let tbody = document.getElementById('id_totals_table');
                        tbody.innerHTML = "";
                        for (let row of response.data) {
                            let tr = document.createElement('tr');
                            let td_name = document.createElement('td');
                            let strong_name = document.createElement('strong');
                            strong_name.textContent = row.name;
                            td_name.appendChild(strong_name);
                            tr.appendChild(td_name);
                            let td_value = document.createElement('td');
                            td_value.className = 'text-monospace text-right';
                            td_value.textContent = thousandSeparateNumber(row.amount);
                            tr.appendChild(td_value);
                            tbody.appendChild(tr);
                        }
                    }
                }).catch(function (ex) {
                    alert("Error occurred. Try again later.");
                    if (window.console)
                        console.log('failed submitting', ex)
                });

                // Fetch details
                fetch(
                    `//${window.location.host}${ev.target.dataset.baseHref}${country_id}/rows.json`
                ).then((response) => response.json()).then(function (response) {
                    if (response.status) {
                        let table = document.getElementById('id_detail_table');
                        let thead = table.querySelector('thead')
                        thead.innerHTML = "";
                        let tbody = table.querySelector('tbody')
                        tbody.innerHTML = "";

                        for (let i = 0; i < response.data.length; i++) {
                            let row = response.data[i]
                            let tr = document.createElement('tr');
                            for (let j = 0; j < row.length; j++) {
                                if (i === 0) {  // Header row
                                    let th = document.createElement('th')
                                    th.textContent = row[j]
                                    tr.appendChild(th)
                                } else {
                                    let td = document.createElement('td')
                                    if (j === 0) {
                                        let strong = document.createElement('strong');
                                        strong.textContent = row[j];
                                        strong.dataset.toggle = 'tooltip'
                                        strong.title = getDateFromEday(row[j]);
                                        $(strong).tooltip();
                                        td.appendChild(strong);
                                    } else {
                                        td.textContent = thousandSeparateNumber(row[j])
                                        td.className = 'text-right';
                                    }
                                    td.classList.add('text-monospace')
                                    tr.appendChild(td)
                                }
                            }
                            if (i === 0)
                                thead.appendChild(tr);
                            else
                                tbody.appendChild(tr);
                        }
                    }
                }).catch(function (ex) {
                    alert("Error occurred. Try again later.");
                    if (window.console)
                        console.log('failed submitting', ex)
                });
                document.getElementById('id_result_container').classList.remove('d-none');
            }

        }

    });
}
